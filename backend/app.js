// require the dependencies we installed
var app = require('express')();
var responseTime = require('response-time')
var axios = require('axios');
var redis = require('redis');
var paypal = require('paypal-rest-sdk');
// create a new redis client and connect to our local redis instance
var client = redis.createClient(6379, 'redis');
var bodyParser = require('body-parser');
var cors = require('cors');
var morgan = require('morgan');
import {logStream} from './logger';
import braintree from 'braintree'
import braintreeClient from 'braintree-web'
var creditCardType = require('credit-card-type');
// if an error occurs, print it to the console
client.on('error', function (err) {
  console.log("Error " + err);
});

// log incoming requests
app.use(morgan('tiny', {stream: logStream}));

app.use(bodyParser.urlencoded({extended: false}));

// parser application/json
app.use(bodyParser.json());

// allow cors
app.use(cors());

app.set('port', (process.env.PORT || 5000));
// set up the response-time middleware
app.use(responseTime());

paypal.configure({
  'mode': 'sandbox', //sandbox or live
  'client_id': 'AUV-Q7h2CGfwVAGRnlCh5WCP3Fm4LCFbbC8pcoZ0uvcQI2W0xvJPowpkuyzPGjGX_aNrmnY_PbRB3bod',
  'client_secret': 'EADr-Papl50LFBivRl9VsDXhDyVmguQwHezZgGdIZpx3A0-kVc0fCDqeo5uRLSEai5vfRVDgBfEX9d6b'
});

var gateway = braintree.connect({
  // accessToken:
  // 'access_token$sandbox$hmdf6nwb9f2nrw23$7aec7a64823fea49505fad4e7284068d'
  environment: braintree.Environment.Sandbox,
  merchantId: 'r2dmpfm3nvgzk9vp',
  publicKey: 'ryjf22dp2nbf4zpr',
  privateKey: '2ab91a789d72db28b5bc34c4dbd55bb0'
});

function createPayPalCreditCardPayment(req, res) {
  switch (req.body.cardType) {
    case "AMEX":
      req.body.cardType = "amex"
      break;
    case "visa":
      req.body.cardType = "visa"
      break;
    case "master-card":
      req.body.cardType = "mastercard"
      break;
    default:
      break;
  }
  console.log('cardType', req.body.cardType)
  let paymentFormat = {
    "intent": "sale",
    "payer": {
      "payment_method": "credit_card",
      "funding_instruments": [
        {
          "credit_card": {
            "type": req.body.cardType,
            "number": req.body.cardNumber,
            "expire_month": req.body.expirationMonth,
            "expire_year": req.body.expirationYear,
            "cvv2": req.body.ccv,
            "first_name": req.body.cardFirstName,
            "last_name": req.body.cardLastName
          }
        }
      ]
    },
    "transactions": [
      {
        "amount": {
          "total": req.body.price,
          "currency": req.body.currency
        },
        "description": JSON.stringify({"name":req.body.name, "phoneNumber":req.body.phoneNumber})
      }
      
    ],
    

  }
  console.log('paymentFormat', paymentFormat)
  paypal
    .payment
    .create(paymentFormat, function (error, payment) {
      if (error) {
        console.log(error)
        res
          .send({"result": "error"})
          .status(400)
      } else {
        console.log("Create Payment Response");
        console.log(payment);
        res.send({"payment":payment})
      }
    });
}

function getPayPalPayment(req, res) {
  paypal
    .payment
    .get(req.params.paymentId, function (error, payment) {
      if (error) {
        console.log(error);
        res
          .send({"result": "error"})
          .status(400)
      } else {
        console.log("Get Payment Response");
        console.log(JSON.stringify(payment));
        client.setex(req.params.paymentId, 60, JSON.stringify(payment));
        res.send({"payment": payment, "source": "PayPal API"})
      }

    });
}
function getBrainTreePayment(req, res) {
  gateway
    .transaction
    .find(req.params.paymentId, function (err, transaction) {
      if (err) {
        console.log(error);

        res
          .send({"result": "error"})
          .status(400)
      } else {
        console.log("Get Payment Response");
        console.log(JSON.stringify(transaction));
        client.setex(req.params.paymentId, 60, JSON.stringify(transaction));
        res.send({"payment": transaction, "source": "BrainTree API"})
      }

    });
}

function createBrainTreeCreditCardPayment(req, res) {
  gateway
    .transaction
    .sale({
      amount: req.body.price,
      paymentMethodNonce: req.body.nonce,
      options: {
        submitForSettlement: true
      },
      customer:{
        lastName:req.body.name,
        phone:req.body.phoneNumber
      }
    }, function (err, result) {
      if (err) {
        res
          .send({"error": error})
          .status(400)
        console.log(err)
      } else {
        res.send({"payment": result.transaction})
        console.log(result)
      }
    });
}

app.get('/', (req, res) => {
  res.send('weMediaTest');
});

app.get('/api/payment/:paymentId', (req, res) => {
  var paymentId = req.params.paymentId;

  client.get(paymentId, function (error, result) {
    if (result) {
      // the result exists in our cache - return it to our user immediately
      res.send({
        "payment": JSON.parse(result),
        "source": "redis cache"
      });
    } else {
      if (paymentId.length > 8) {
        getPayPalPayment(req, res)
      } else {
        getBrainTreePayment(req, res)
      }

    }
  })

})

app.post('/api/payment', (req, res) => {
  console.log('req', req.body)
  req.body["cardType"] = creditCardType(req.body.cardNumber)[0].type

  if (creditCardType(req.body.cardNumber)[0].type == 'AMEX' && !req.body.currency == 'USD') {
    res
      .send({"result": "error"})
      .status(400)
    return
  }
  if (req.body.currency == 'USD' || req.body.currency == 'EUR' || req.body.currency == 'AUD') {

    createPayPalCreditCardPayment(req, res)

  } else {
    createBrainTreeCreditCardPayment(req, res)
    //  res.send(req.body)
  }

});

app.post("/checkout", function (req, res) {
  var nonce = req.body.payment_method_nonce;
  // Use payment method nonce here
});

app.get("/client_token", function (req, res) {
  gateway
    .clientToken
    .generate({}, function (err, response) {
      res.send(response.clientToken);
    });
});

app.listen(app.get('port'), function () {
  console.log('Server listening on port: ', app.get('port'));
});