/*
 *
 * CreditCardPayment
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import makeSelectCreditCardPayment,{makeSelectResult } from './selectors';
import messages from './messages';
import {submitPayment} from './actions'
import braintree from 'braintree-web'

import {
  Tab,
  Tabs,
  Form,
  FormControl,
  FormGroup,
  Col,
  Button,
  Checkbox,
  ControlLabel,
  InputGroup,
  DropdownButton,
  MenuItem,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'react-bootstrap'
import {appCurrency} from '../../support'
import NavBar from '../../components/NavBar'


function CurrencyList(props) {
  let content = (
    <DropdownButton
      componentClass={InputGroup.Button}
      id="input-dropdown-addon"
      title="Currency"></DropdownButton>
  );
  content = appCurrency.map((item, index) => (
    <MenuItem eventKey={item} key={index}>{item}
    </MenuItem>
  ))
  return (
    <DropdownButton
      componentClass={InputGroup.Button}
      id="input-dropdown-addon"
      title="Currency"
      onSelect={(eventKey, event) => props.onChangeCurrency(eventKey)}>{content}</DropdownButton>
  );
}

export class CreditCardPayment extends React.Component { // eslint-disable-line react/prefer-stateless-function
  state = {
    key: 1,
    currency: '',
    name:'',
    phoneNumber:'',
    price:'',
    cardFirstName:'',
    cardLastName:'',
    cardNumber:'',
    expirationMonth:'',
    expirationYear:'',
    ccv:'',
    clientToken:null,
    showDialog:false,
  }
  onChangeCurrency(currencyName) {
    this.setState({currency: currencyName})
  }
  onChangeName(event){
    this.setState({name:event.target.value})
  }
  
  onChangePhoneNumber(event){
    this.setState({phoneNumber:event.target.value})
  }

  onChangePrice(event){
    this.setState({price:event.target.value})
  }

  onChangeCardFirstName(event){
    this.setState({cardFirstName:event.target.value})
  }
   onChangeCardLastName(event){
    this.setState({cardLastName:event.target.value})
  }
  onChangeCardNumber(event){
    this.setState({cardNumber:event.target.value})
  }

  onChangeExpirationMonth(event){
    this.setState({expirationMonth:event.target.value})
  }

  onChangeExpirationYear(event){
    this.setState({expirationYear:event.target.value})
  }

  onChangeCCV(event){
    this.setState({ccv:event.target.value})
  }

  handleSelect(key) {
    this.setState({key});
  }
  onSubmitPayment(){
    // this.getNonce()
    this.setState({showDialog:true})
    this.props.dispatch(submitPayment(this.state.name, this.state.phoneNumber, this.state.currency, this.state.price, this.state.cardFirstName,this.state.cardLastName, this.state.cardNumber, this.state.expirationMonth, this.state.expirationYear, this.state.ccv))
    
  }

   hideModal() {
    this.setState({showDialog: false});
  }
  componentWillMount(){
    
   
    
  }
  render() {
    return (
      <div>
        <NavBar/>
        <h1>
          Credit Card Payment
        </h1>
        <Tabs
          activeKey={this.state.key}
          onSelect={this
          .handleSelect
          .bind(this)}
          id="controlled-tab-example">
          <Tab eventKey={1} title="Order Section">
            <Form horizontal>
              <FormGroup controlId="formHorizontalName">
                <Col componentClass={ControlLabel} sm={2}>
                  Name
                </Col>
                <Col sm={6}>
                  <FormControl value={this.state.name} onChange={this.onChangeName.bind(this)} type="text" placeholder="Name"/>
                </Col>
              </FormGroup>

              <FormGroup controlId="formHorizontalPhoneNumber">
                <Col componentClass={ControlLabel} sm={2}>
                  Phone Number
                </Col>
                <Col sm={6}>
                  <FormControl value={this.state.phoneNumber} onChange={this.onChangePhoneNumber.bind(this)} type="number" placeholder="Phone Number"/>
                </Col>
              </FormGroup>

              <FormGroup>
                <Col componentClass={ControlLabel} sm={2}>
                  Currency
                </Col>
                <Col sm={6}>
                <InputGroup>
                  <FormControl type="text" value={this.state.currency} disabled/>

                  <CurrencyList
                    onChangeCurrency={this
                    .onChangeCurrency
                    .bind(this)}/>

                </InputGroup>
                </Col>
                
              </FormGroup>

              <FormGroup controlId="formHorizontalPrice">
                <Col componentClass={ControlLabel} sm={2}>
                  Price
                </Col>
                <Col sm={6}>
                  <FormControl value={this.state.price} onChange={this.onChangePrice.bind(this)} type="number" placeholder="Price"/>
                </Col>
              </FormGroup>

              {/*<FormGroup>
                <Col smOffset={2} sm={10}>
                  <Checkbox>Remember me</Checkbox>
                </Col>
              </FormGroup>*/}

              <FormGroup>
                <Col smOffset={2} sm={10}>
                  <Button onClick={()=>this.handleSelect(2)}>
                    Next
                  </Button>
                </Col>
              </FormGroup>
            </Form>

          </Tab>
          <Tab eventKey={2} title="Payment Section">
            <Form horizontal >
              <FormGroup controlId="formHorizontalCreditCardHolderName">
                <Col componentClass={ControlLabel} sm={2}>
                  Credit Card Holder Name
                </Col>
                <Col sm={3}>
                  <FormControl value={this.state.cardFirstName} onChange={this.onChangeCardFirstName.bind(this)} type="text" placeholder="Credit Card Holder First Name"/>
                </Col>
                <Col sm={3}>
                  <FormControl value={this.state.cardLastName} onChange={this.onChangeCardLastName.bind(this)} type="text" placeholder="Credit Card Holder Last Name"/>
                </Col>
              </FormGroup>

              

              <FormGroup controlId="formHorizontalCreditCardNumber">
                <Col componentClass={ControlLabel} sm={2}>
                  Credit Card Number
                </Col>
                <Col sm={6}>
                  <FormControl value={this.state.cardNumber} onChange={this.onChangeCardNumber.bind(this)} type="number" placeholder="Credit Card Number"/>
                </Col>
              </FormGroup>

              <FormGroup controlId="formHorizontalExpiration">
                <Col componentClass={ControlLabel} sm={2}>
                  Expiration Date
                </Col>
                <Col sm={3}>
                  <FormControl value={this.state.expirationMonth} onChange={this.onChangeExpirationMonth.bind(this)} type="number" placeholder="Month"/>
                </Col>
                <Col sm={3}>
                  <FormControl value={this.state.expirationYear} onChange={this.onChangeExpirationYear.bind(this)} type="number" placeholder="Year"/>
                </Col>
              </FormGroup>
              

              <FormGroup controlId="formHorizontalCCV">
                <Col componentClass={ControlLabel} sm={2}>
                  CCV
                </Col>
                <Col sm={3}>
                  <FormControl value={this.state.ccv} onChange={this.onChangeCCV.bind(this)} type="number" placeholder="CCV"/>
                </Col>
              </FormGroup>

              {/*<FormGroup>
                <Col smOffset={2} sm={10}>
                  <Checkbox>Remember me</Checkbox>
                </Col>
              </FormGroup>*/}

              <FormGroup>
                <Col smOffset={2} sm={10}>
                  <Button onClick={this.onSubmitPayment.bind(this)}>
                    Submit Payment
                  </Button>
                </Col>
              </FormGroup>
            </Form>
           
          </Tab>
          
        </Tabs>
        <Modal show={this.state.showDialog} onHide={this.hideModal.bind(this)}>
       
      <Modal.Header >
        <Modal.Title>Loading</Modal.Title>
      </Modal.Header>

      <Modal.Body>
       PaymentID: {this.props.result}
      </Modal.Body>

      

    
    </Modal>
      </div>
    );
  }
}

CreditCardPayment.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  CreditCardPayment: makeSelectCreditCardPayment(),
  result:makeSelectResult (),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CreditCardPayment);
