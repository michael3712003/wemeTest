/*
 * CreditCardPayment Messages
 *
 * This contains all the text for the CreditCardPayment component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.CreditCardPayment.header',
    defaultMessage: 'This is CreditCardPayment container !',
  },
});
