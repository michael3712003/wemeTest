/*
 *
 * CreditCardPayment actions
 *
 */

import {
  DEFAULT_ACTION,
  SUBMIT_PAYMENT,
    SUBMIT_PAYMENT_FAILURE,
    SUBMIT_PAYMENT_SUCCESS
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
    
  };
}

export function submitPayment(name, phoneNumber, currency, price, cardFirstName, cardLastName,cardNumber, expirationMonth, expirationYear, ccv) {
  return {
    type: SUBMIT_PAYMENT,
    currency,
    name,
    phoneNumber,
    price,
    cardFirstName, 
    cardLastName,
    cardNumber,
    expirationMonth,
    expirationYear,
    ccv
  };
}

export function submitPaymentSuccess(result) {
  return {
    type: SUBMIT_PAYMENT_SUCCESS,
    result
  };
}

export function submitPaymentFailure() {
  return {
    type: SUBMIT_PAYMENT_FAILURE,
  };
}
