import { createSelector } from 'reselect';

/**
 * Direct selector to the creditCardPayment state domain
 */
const selectCreditCardPaymentDomain = () => (state) => state.get('creditCardPayment');

/**
 * Other specific selectors
 */


/**
 * Default selector used by CreditCardPayment
 */

const makeSelectCreditCardPayment = () => createSelector(
  selectCreditCardPaymentDomain(),
  (substate) => substate.toJS()
);

const makeSelectResult = () => createSelector(
  selectCreditCardPaymentDomain(),
  (substate) => substate.get('result')
);

export default makeSelectCreditCardPayment;
export {
  makeSelectResult,
  selectCreditCardPaymentDomain,
};
