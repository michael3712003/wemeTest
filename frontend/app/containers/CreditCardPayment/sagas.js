import {take, call, put, select, takeLatest} from 'redux-saga/effects';
import {apiURL} from '../../support'
import {SUBMIT_PAYMENT} from './constants'
import {submitPaymentSuccess, submitPaymentFailure} from './actions'
import {LOCATION_CHANGE} from 'react-router-redux';
import braintree from 'braintree-web'
import creditCardType from 'credit-card-type';
// Individual exports for testing
export function * defaultSaga() {
  // See example in containers/HomePage/sagas.js
}

function submitPayment(action, nonce) {
  return fetch(`${apiURL}/api/payment`, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({

      "name": action.name,
      "phoneNumber": action.phoneNumber,
      "currency": action.currency,
      "price": action.price,
      "cardFirstName": action.cardFirstName,
      "cardLastName": action.cardLastName,
      "cardNumber": action.cardNumber,
      "expirationMonth": action.expirationMonth,
      "expirationYear": action.expirationYear,
      "ccv": action.ccv,
      "nonce": nonce

    })
  }).then((response) => response.json()).then((responseJson) => {
    return responseJson;
  })

}

function getNonce(action, cardType) {

  let data = {
    creditCard: {
      number: action.cardNumber,
      expirationDate: `${action
        .expirationMonth}/${action
        .expirationYear
        .slice(-2)}`,
      cvv: action.ccv,
      billingAddress: {
        postalCode: '12345'
      }
    }
  }
  function braintreeNonce(resolve, reject,responseJson){
    braintree
      .client
      .create({
        authorization: responseJson
      }, function (err, clientInstance) {
        // console.log(state)
        console.log(action.cardNumber, action.expirationMonth, action.expirationYear.slice(-2), action.ccv)
         clientInstance.request({
          endpoint: 'payment_methods/credit_cards',
          method: 'post',
          data: data
        }, function (err, response) {
          if (err) {
            console.log(err)
            reject(err)
          }
          else {
            console.log('nonce', response.creditCards[0].nonce)
            resolve(response.creditCards[0].nonce)
          }

        });
      
      });
  }

  // data.creditCard[`${cardType}`]=action.ccv console.log('data',data)
  return fetch('http://localhost:5000/client_token').then((response) => response.text()).then((responseJson) => {
    return new Promise((resolve, reject)=>{
       braintreeNonce(resolve, reject,responseJson)
    })
    
  })
}

export function * submitPaymentSaga(action) {
  const cardType = creditCardType(action.cardNumber)[0].code.name
  console.log('cardType', cardType)
  try {
    const nonce = yield getNonce(action, cardType)
    console.log('nonce', nonce)
    try {    const result =yield submitPayment(action, nonce)
    console.log('result', result.payment.id) 
    yield put(submitPaymentSuccess(result.payment.id))
    } 
    catch (error) {
      yield put(submitPaymentFailure())
    console.log('errorSubmit',error) }
    alert('errorSubmit')
  } catch (error) {
    console.log('error geNonce', error)
    alert('error geNonce')
  }
}

export function * watchSubmitPayment() {
  const watcher = yield takeLatest(SUBMIT_PAYMENT, submitPaymentSaga);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

// All sagas to be loaded
export default[watchSubmitPayment,
  defaultSaga];
