/*
 *
 * CreditCardPayment constants
 *
 */

export const DEFAULT_ACTION = 'app/CreditCardPayment/DEFAULT_ACTION';


export const SUBMIT_PAYMENT = 'app/CreditCardPayment/SUBMIT_PAYMENT';

export const SUBMIT_PAYMENT_SUCCESS = 'app/CreditCardPayment/SUBMIT_PAYMENT_SUCCESS';

export const SUBMIT_PAYMENT_FAILURE = 'app/CreditCardPayment/SUBMIT_PAYMENT_FAILURE';