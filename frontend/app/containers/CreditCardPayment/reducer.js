/*
 *
 * CreditCardPayment reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  SUBMIT_PAYMENT,
  SUBMIT_PAYMENT_FAILURE,
  SUBMIT_PAYMENT_SUCCESS
} from './constants';

const initialState = fromJS({
  result:null
});

function creditCardPaymentReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case SUBMIT_PAYMENT:
      return state.set('result',null)
    case SUBMIT_PAYMENT_SUCCESS:
      return state.set('result', action.result)
    case SUBMIT_PAYMENT_FAILURE:
      return state.set('result','error')
    default:
      return state;
  }
}

export default creditCardPaymentReducer;
