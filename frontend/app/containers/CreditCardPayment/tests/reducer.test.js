
import { fromJS } from 'immutable';
import creditCardPaymentReducer from '../reducer';

describe('creditCardPaymentReducer', () => {
  it('returns the initial state', () => {
    expect(creditCardPaymentReducer(undefined, {})).toEqual(fromJS({}));
  });
});
