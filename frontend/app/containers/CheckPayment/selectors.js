import { createSelector } from 'reselect';

/**
 * Direct selector to the checkPayment state domain
 */
const selectCheckPaymentDomain = () => (state) => state.get('checkPayment');

/**
 * Other specific selectors
 */


/**
 * Default selector used by CheckPayment
 */

const makeSelectCheckPayment = () => createSelector(
  selectCheckPaymentDomain(),
  (substate) => substate.toJS()
);

 const makeSelectResult =()=> createSelector(
  selectCheckPaymentDomain(),
  (substate) => substate.get('result')
);

export default makeSelectCheckPayment;
export {
  makeSelectResult,
  selectCheckPaymentDomain,
};
