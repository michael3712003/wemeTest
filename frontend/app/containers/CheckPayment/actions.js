/*
 *
 * CheckPayment actions
 *
 */

import {
  DEFAULT_ACTION,
  SUBMIT_CHECK_PAYMENT,
  SUBMIT_CHECK_PAYMENT_FAILURE,
  SUBMIT_CHECK_PAYMENT_SUCCESS
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function submitCheckPayment(customerName, paymentID){
  return{
    type: SUBMIT_CHECK_PAYMENT,
    customerName,
    paymentID
  }
}

export function submitCheckPaymentSuccess(result){
  return{
    type: SUBMIT_CHECK_PAYMENT_SUCCESS,
    result
  }
}

export function submitCheckPaymentFailure(){
  return{
    type: SUBMIT_CHECK_PAYMENT_FAILURE,
  }
}
