
import { fromJS } from 'immutable';
import checkPaymentReducer from '../reducer';

describe('checkPaymentReducer', () => {
  it('returns the initial state', () => {
    expect(checkPaymentReducer(undefined, {})).toEqual(fromJS({}));
  });
});
