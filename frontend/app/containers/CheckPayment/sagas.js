import { take, call, put, select , takeLatest} from 'redux-saga/effects';
import {apiURL} from '../../support'
import {SUBMIT_CHECK_PAYMENT} from './constants'
import {submitCheckPaymentSuccess, submitCheckPaymentFailure} from './actions'
import { LOCATION_CHANGE } from 'react-router-redux';
// Individual exports for testing
export function* defaultSaga() {
  // See example in containers/HomePage/sagas.js
}

function getPaymentID(customerName,paymentID){
  return fetch(`${apiURL}/api/payment/${paymentID}`).then((response) => response.json())
      .then((responseJson) => {
        return responseJson;
      })
}

export function* checkPaymentSaga(action){
  try {
    const result =yield getPaymentID(action.customerName, action.paymentID)
    console.log("result",result)
    let payment
    if(action.paymentID.length>8){
        payment={
          amount:result.payment.transactions[0].amount.total,
          currency:result.payment.transactions[0].amount.currency,
          name:JSON.parse( result.payment.transactions[0].description).name,
          phone:JSON.parse( result.payment.transactions[0].description).phoneNumber
        }
        
      }else{
        payment={
          amount:result.payment.amount,
          currency:result.payment.currencyIsoCode,
          name:result.payment.customer.lastName,
          phone:result.payment.customer.phone
        }
      }
    console.log("result payment", payment)
    yield put(submitCheckPaymentSuccess(payment))
  } catch (error) {
    
    console.log(error)
    yield put(submitCheckPaymentFailure())
  }
    
}

export function* watchSubmitCheckPayment(){
  const watcher = yield takeLatest(SUBMIT_CHECK_PAYMENT, checkPaymentSaga);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

// All sagas to be loaded
export default [
  watchSubmitCheckPayment,
  defaultSaga,
];
