/*
 * CheckPayment Messages
 *
 * This contains all the text for the CheckPayment component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.CheckPayment.header',
    defaultMessage: 'This is CheckPayment container !',
  },
});
