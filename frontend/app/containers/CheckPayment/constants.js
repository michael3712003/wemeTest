/*
 *
 * CheckPayment constants
 *
 */

export const DEFAULT_ACTION = 'app/CheckPayment/DEFAULT_ACTION';

export const SUBMIT_CHECK_PAYMENT = 'app/CheckPayment/SUBMIT_CHECK_PAYMENT';

export const SUBMIT_CHECK_PAYMENT_SUCCESS = 'app/CheckPayment/SUBMIT_CHECK_PAYMENT_SUCCESS';

export const SUBMIT_CHECK_PAYMENT_FAILURE = 'app/CheckPayment/SUBMIT_CHECK_PAYMENT_FAILURE';
