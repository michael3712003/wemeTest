/*
 *
 * CheckPayment
 *
 */

import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import Helmet from 'react-helmet';
import {FormattedMessage} from 'react-intl';
import {createStructuredSelector} from 'reselect';
import makeSelectCheckPayment,{makeSelectResult} from './selectors';
import messages from './messages';
import NavBar from '../../components/NavBar'
import {Form, FormControl, FormGroup, Col, ControlLabel, Button, Modal, ModalBody, ModalFooter, ModalHeader, ModalTitle} from 'react-bootstrap'
import {submitCheckPayment} from './actions'
export class CheckPayment extends React.Component { // eslint-disable-line react/prefer-stateless-function
  state={
    customerName:'',
    paymentID:'',
    showDialog:false,
  }
  handleSubmit(e){
    e.preventDefault()
    this.props.dispatch(submitCheckPayment(this.state.customerName, this.state.paymentID))
    this.setState({showDialog:true})
  }
  hideModal() {
    this.setState({showDialog: false});
  }
  render() {
    return (
      <div>
        <NavBar/>
        <h1>
          Check Payment</h1>
        <Form horizontal onSubmit={this.handleSubmit.bind(this)}>
          <FormGroup controlId="formHorizontalName">
            <Col componentClass={ControlLabel} sm={2}>
              Name
            </Col>
            <Col sm={6}>
              <FormControl type="text" placeholder="Name" value={this.state.customerName} onChange={this.onChangeCustomerName}/>
            </Col>
          </FormGroup>

          <FormGroup controlId="formHorizontalPaymentID">
            <Col componentClass={ControlLabel} sm={2}>
              Payment ID
            </Col>
            <Col sm={6}>
              <FormControl type="text" placeholder="PaymentID" value={this.state.paymentID} onChange={this.onChangePaymentID}/>
            </Col>
          </FormGroup>
          <FormGroup>
                <Col smOffset={2} sm={10}>
                  <Button type="submit">
                    Submit
                  </Button>
                </Col>
              </FormGroup>
        </Form>

        <Modal show={this.state.showDialog} onHide={this.hideModal.bind(this)}>
       
      <Modal.Header >
        <Modal.Title>Payment Detail</Modal.Title>
      </Modal.Header>

      <Modal.Body>
       Name: {this.props.result?this.props.result.name:""}
       <br/>
       Phone: {this.props.result?this.props.result.phone:""}
       <br/>
       Currency:{this.props.result?this.props.result.currency:""}
       <br/>
       Amount:{this.props.result?this.props.result.amount:""}
       <br/>
      </Modal.Body>
    
    </Modal>
      </div>
    );
  }
  onChangeCustomerName=(event)=>{
    this.setState({customerName:event.target.value})
  }
  onChangePaymentID=(event)=>{
    this.setState({paymentID:event.target.value})
  }
}

CheckPayment.propTypes = {
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
  CheckPayment: makeSelectCheckPayment(),
  result:makeSelectResult()
});

function mapDispatchToProps(dispatch) {
  return {dispatch};
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckPayment);
