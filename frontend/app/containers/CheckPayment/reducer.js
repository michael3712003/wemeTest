/*
 *
 * CheckPayment reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  SUBMIT_CHECK_PAYMENT,
  SUBMIT_CHECK_PAYMENT_FAILURE,
  SUBMIT_CHECK_PAYMENT_SUCCESS,
} from './constants';

const initialState = fromJS({
  result:null
});

function checkPaymentReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case SUBMIT_CHECK_PAYMENT:
      return state.set('result',null);
    case SUBMIT_CHECK_PAYMENT_SUCCESS:
      return state.set('result',action.result);
    case SUBMIT_CHECK_PAYMENT_FAILURE:
      return state.set('result','Record No Found Message');
    default:
      return state;
  }
}

export default checkPaymentReducer;
