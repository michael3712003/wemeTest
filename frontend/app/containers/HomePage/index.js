/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import {FormattedMessage} from 'react-intl';
import messages from './messages';
import {
  Tab,
  Tabs,
  Form,
  FormControl,
  FormGroup,
  Col,
  Button,
  Checkbox,
  ControlLabel,
  InputGroup,
  DropdownButton,
  MenuItem
} from 'react-bootstrap'
import {appCurrency} from '../../support'
import NavBar from '../../components/NavBar'
function CurrencyList(props) {
  let content = (
    <DropdownButton
      componentClass={InputGroup.Button}
      id="input-dropdown-addon"
      title="Currency"></DropdownButton>
  );
  content = appCurrency.map((item, index) => (
    <MenuItem eventKey={item} key={index}>{item}
    </MenuItem>
  ))
  return (
    <DropdownButton
      componentClass={InputGroup.Button}
      id="input-dropdown-addon"
      title="Currency"
      onSelect={(eventKey, event) => props.onChangeCurrency(eventKey)}>{content}</DropdownButton>
  );
}
export default class HomePage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  state = {
    key: 1,
    currency: '',
    name:'',
    phoneNumber:'',
    price:'',
    cardName:'',
    expirationMonth:'',
    expirationYear:'',
    ccv:''
  }
  onChangeCurrency(currencyName) {
    this.setState({currency: currencyName})
  }
  onChangeName(event){

  }

  handleSelect(key) {
    this.setState({key});
  }
  render() {
    return (
      <div>
        <NavBar/>
        <h1>
          Credit Card Payment
        </h1>
        <Tabs
          activeKey={this.state.key}
          onSelect={this
          .handleSelect
          .bind(this)}
          id="controlled-tab-example">
          <Tab eventKey={1} title="Order Section">
            <Form horizontal>
              <FormGroup controlId="formHorizontalName">
                <Col componentClass={ControlLabel} sm={2}>
                  Name
                </Col>
                <Col sm={6}>
                  <FormControl type="text" placeholder="Name"/>
                </Col>
              </FormGroup>

              <FormGroup controlId="formHorizontalPhoneNumber">
                <Col componentClass={ControlLabel} sm={2}>
                  Phone Number
                </Col>
                <Col sm={6}>
                  <FormControl type="number" placeholder="Phone Number"/>
                </Col>
              </FormGroup>

              <FormGroup>
                <Col componentClass={ControlLabel} sm={2}>
                  Currency
                </Col>
                <Col sm={6}>
                <InputGroup>
                  <FormControl type="text" value={this.state.currency} disabled/>

                  <CurrencyList
                    onChangeCurrency={this
                    .onChangeCurrency
                    .bind(this)}/>

                </InputGroup>
                </Col>
                
              </FormGroup>

              <FormGroup controlId="formHorizontalPrice">
                <Col componentClass={ControlLabel} sm={2}>
                  Price
                </Col>
                <Col sm={6}>
                  <FormControl type="number" placeholder="Price"/>
                </Col>
              </FormGroup>

              {/*<FormGroup>
                <Col smOffset={2} sm={10}>
                  <Checkbox>Remember me</Checkbox>
                </Col>
              </FormGroup>*/}

              <FormGroup>
                <Col smOffset={2} sm={10}>
                  <Button type="submit">
                    Next
                  </Button>
                </Col>
              </FormGroup>
            </Form>

          </Tab>
          <Tab eventKey={2} title="Payment Section">
            <Form horizontal>
              <FormGroup controlId="formHorizontalCreditCardHolderName">
                <Col componentClass={ControlLabel} sm={2}>
                  Credit Card Holder Name
                </Col>
                <Col sm={6}>
                  <FormControl type="text" placeholder="Credit Card Holder Name"/>
                </Col>
              </FormGroup>

              <FormGroup controlId="formHorizontalCreditCardNumber">
                <Col componentClass={ControlLabel} sm={2}>
                  Credit Card Number
                </Col>
                <Col sm={6}>
                  <FormControl type="number" placeholder="Credit Card Number"/>
                </Col>
              </FormGroup>

              <FormGroup controlId="formHorizontalCreditCardNumber">
                <Col componentClass={ControlLabel} sm={2}>
                  Expiration Date
                </Col>
                <Col sm={3}>
                  <FormControl type="number" placeholder="Month"/>
                </Col>
                <Col sm={3}>
                  <FormControl type="number" placeholder="Year"/>
                </Col>
              </FormGroup>
              

              <FormGroup controlId="formHorizontalPrice">
                <Col componentClass={ControlLabel} sm={2}>
                  CCV
                </Col>
                <Col sm={3}>
                  <FormControl type="number" placeholder="CCV"/>
                </Col>
              </FormGroup>

              {/*<FormGroup>
                <Col smOffset={2} sm={10}>
                  <Checkbox>Remember me</Checkbox>
                </Col>
              </FormGroup>*/}

              <FormGroup>
                <Col smOffset={2} sm={10}>
                  <Button type="submit">
                    Submit Payment
                  </Button>
                </Col>
              </FormGroup>
            </Form>
          </Tab>
          
        </Tabs>
      </div>
    );
  }
}
