/**
*
* NavBar
*
*/

import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import {Nav, Navbar, NavItem, NavDropdown, MenuItem} from 'react-bootstrap'
class NavBar extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <Navbar>
        <Navbar.Header>
          <Navbar.Brand>
            <a href="/">Home</a>
          </Navbar.Brand>
        </Navbar.Header>
        <Nav>
          <NavItem eventKey={1} href="/">Payment</NavItem>
          <NavItem eventKey={2} href="/checkPayment">Check Payment</NavItem>
        </Nav>
      </Navbar>
    );
  }
}

NavBar.propTypes = {

};

export default NavBar;
